package com.yuwx.base.service;

import java.util.List;

import com.baomidou.framework.service.ISuperService;
import com.yuwx.commons.result.Tree;
import com.yuwx.base.model.Organization;

/**
 *
 * Organization 表数据服务层接口
 *
 */
public interface IOrganizationService extends ISuperService<Organization> {

    List<Tree> selectTree();

    List<Organization> selectTreeGrid();

}