package com.yuwx.base.service;

import com.yuwx.base.model.UserRole;
import com.baomidou.framework.service.ISuperService;

/**
 *
 * UserRole 表数据服务层接口
 *
 */
public interface IUserRoleService extends ISuperService<UserRole> {


}