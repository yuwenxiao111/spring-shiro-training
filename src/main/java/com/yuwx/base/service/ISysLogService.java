package com.yuwx.base.service;

import com.yuwx.commons.utils.PageInfo;
import com.yuwx.base.model.SysLog;
import com.baomidou.framework.service.ISuperService;

/**
 *
 * SysLog 表数据服务层接口
 *
 */
public interface ISysLogService extends ISuperService<SysLog> {

    void selectDataGrid(PageInfo pageInfo);


}