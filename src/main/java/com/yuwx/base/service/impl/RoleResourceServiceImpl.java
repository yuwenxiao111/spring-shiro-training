package com.yuwx.base.service.impl;

import org.springframework.stereotype.Service;

import com.yuwx.base.dao.RoleResourceMapper;
import com.yuwx.base.model.RoleResource;
import com.yuwx.base.service.IRoleResourceService;
import com.baomidou.framework.service.impl.SuperServiceImpl;

/**
 *
 * RoleResource 表数据服务层接口实现类
 *
 */
@Service
public class RoleResourceServiceImpl extends SuperServiceImpl<RoleResourceMapper, RoleResource> implements IRoleResourceService {


}