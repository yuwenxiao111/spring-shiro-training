package com.yuwx.base.service;

import java.util.List;

import com.baomidou.framework.service.ISuperService;
import com.yuwx.commons.result.Tree;
import com.yuwx.base.model.Resource;
import com.yuwx.base.model.User;

/**
 *
 * Resource 表数据服务层接口
 *
 */
public interface IResourceService extends ISuperService<Resource> {

    List<Resource> selectAll();

    List<Tree> selectAllTree();

    List<Tree> selectAllTrees();

    List<Tree> selectTree(User currentUser);

}