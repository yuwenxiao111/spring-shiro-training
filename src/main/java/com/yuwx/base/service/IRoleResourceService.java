package com.yuwx.base.service;

import com.yuwx.base.model.RoleResource;
import com.baomidou.framework.service.ISuperService;

/**
 *
 * RoleResource 表数据服务层接口
 *
 */
public interface IRoleResourceService extends ISuperService<RoleResource> {


}