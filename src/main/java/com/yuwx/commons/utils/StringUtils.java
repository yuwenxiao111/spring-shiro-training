package com.yuwx.commons.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringEscapeUtils;

/**
 * 继承自Spring util的工具类，减少jar依赖
 * @author L.cm
 */
public class StringUtils extends org.springframework.util.StringUtils {

   

    /**
	 * 
	 * 方法描述：根据浏览器对字符串进行转码
	 * <p/>
	 * @param request
	 * @param str 需要解码的字符串
	 * @return  
	 * <p/>
	 * 创建者：yuwx
	 * <p/> 
	 * 创建时间：2016年6月15日 下午1:15:01
	 * @throws UnsupportedEncodingException 
	 */
	public static String encoderByExplorer(HttpServletRequest request,
			String str) throws UnsupportedEncodingException {
		// 判断浏览器，使用不同编码
		try {
			String userAgent = request.getHeader("USER-AGENT");
			if (org.apache.commons.lang.StringUtils.contains(userAgent, "MSIE")) {// IE浏览器
				str = URLEncoder.encode(str, "UTF8");
			} else if (org.apache.commons.lang.StringUtils.contains(userAgent, "Mozilla")) {// google,火狐浏览器
				str = new String(str.getBytes(), "ISO8859-1");
			} else {
				str = URLEncoder.encode(str, "UTF8");// 其他浏览器
			}
		} catch (UnsupportedEncodingException e) {
			throw e;
		}
		return str;
	}
	/**
     * 半角转全角
     * @param input String.
     * @return 全角字符串.
     */
    public static String ToSBC(String input) {
             char c[] = input.toCharArray();
             for (int i = 0; i < c.length; i++) {
               if (c[i] == ' ') {
                 c[i] = '\u3000';
               } else if (c[i] < '\177') {
                 c[i] = (char) (c[i] + 65248);

               }
             }
             return new String(c);
    }

    /**
     * 全角转半角
     * @param input String.
     * @return 半角字符串
     */
    public static String ToDBC(String input) {
        

             char c[] = input.toCharArray();
             for (int i = 0; i < c.length; i++) {
               if (c[i] == '\u3000') {
                 c[i] = ' ';
               } else if (c[i] > '\uFF00' && c[i] < '\uFF5F') {
                 c[i] = (char) (c[i] - 65248);

               }
             }
        String returnString = new String(c);
        
             return returnString;
    }
    /**
     * 字符串转为Unicode
     * @param str
     * @return
     */
    public static String StringConvertUnicode(String str){
    	return StringEscapeUtils.unescapeJava(str);
    }
    /**
     * 
     * @param unicode转字符串
     * @return
     */
    public static String UnicodeConvertString(String str){
    	return StringEscapeUtils.unescapeJava(str);
    }
    public static void main(String[] args) {
    	System.out.println(
    	UnicodeConvertString("\u4E2D\u56FD"));
	}
    /**
     * 对url中文参数进行编码
     */
    public static String UrlEncode(String str){
    	String param =  "";
    	try {
			 param =  URLEncoder.encode(str, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			System.out.println("url编码错误");
		}
    	return param;
    }
    /**
     * 对url中文参数进行解码，js页面需要对参数编码两次
     * window.location.href="${base}/test/testDecode?name="+encodeURI(encodeURI("于文晓"));
     * @param str
     * @return
     */
    public static String UrlDecode(String str){
    	String param =  "";
    	try {
			 param =  URLDecoder.decode(str, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			System.out.println("url解码错误");
		}
    	return param;
    }
}
